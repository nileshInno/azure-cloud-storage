package com.abc.Imagesoncloddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImagesoncloddemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImagesoncloddemoApplication.class, args);
	}

}
